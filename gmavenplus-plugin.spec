Name:          gmavenplus-plugin
Version:       3.0.2
Release:       1
Summary:       Integrates Groovy into Maven projects
License:       ASL 2.0
URL:           http://groovy.github.io/GMavenPlus/
Source0:       https://github.com/groovy/GMavenPlus/archive/%{version}/GMavenPlus-%{version}.tar.gz

BuildRequires: maven-local mvn(jline:jline) mvn(junit:junit) mvn(org.apache.ant:ant)
BuildRequires: mvn(org.apache.ant:ant-antlr) mvn(org.apache.ant:ant-junit)
BuildRequires: mvn(org.apache.ant:ant-launcher) mvn(org.apache.ivy:ivy)
BuildRequires: mvn(org.apache.maven:maven-core) mvn(org.apache.maven:maven-plugin-api)
BuildRequires: mvn(org.apache.maven:maven-plugin-registry) mvn(org.apache.maven:maven-project)
BuildRequires: mvn(org.apache.maven.plugins:maven-invoker-plugin)
BuildRequires: mvn(org.apache.maven.plugins:maven-plugin-plugin)
BuildRequires: mvn(org.apache.maven.shared:file-management) mvn(org.codehaus:codehaus-parent:pom:)
BuildRequires: mvn(org.codehaus.groovy:groovy-all) mvn(org.codehaus.groovy:groovy-ant)
BuildRequires: mvn(org.codehaus.plexus:plexus-classworlds) mvn(org.codehaus.plexus:plexus-cli)
BuildRequires: mvn(org.codehaus.plexus:plexus-component-metadata)
BuildRequires: mvn(org.codehaus.plexus:plexus-container-default)
BuildRequires: mvn(org.fusesource.jansi:jansi) mvn(org.mockito:mockito-all)
BuildRequires: mvn(ch.qos.logback:logback-classic)
BuildRequires: mvn(org.apache.maven.plugin-tools:maven-plugin-annotations)
BuildRequires: mvn(org.codehaus.plexus:plexus-utils) 
BuildRequires: mvn(org.jacoco:jacoco-maven-plugin)
BuildRequires: mvn(com.google.guava:guava)
BuildArch:     noarch

%description
GMavenPlus which is rewrite of GMaven and allows to integrate Groovy into Maven projects.

%package help
Summary:       Javadoc for gmavenplus-plugin
Provides:      gmavenplus-plugin-javadoc = %{version}-%{release}
Obsoletes:     gmavenplus-plugin-javadoc < %{version}-%{release}

%description help
This package contains javadoc for gmavenplus-plugin.

%prep
%autosetup -n GMavenPlus-%{version} -p1

%pom_remove_plugin :maven-clean-plugin
%pom_remove_plugin :maven-dependency-plugin
%pom_remove_plugin :maven-help-plugin
%pom_remove_plugin :animal-sniffer-maven-plugin
%pom_remove_plugin org.apache.maven.plugins:maven-enforcer-plugin
%pom_remove_plugin :maven-source-plugin
%pom_remove_plugin :maven-deploy-plugin
%pom_remove_plugin :maven-site-plugin

%pom_xpath_remove "pom:build/pom:extensions"

rm -r src/test/java/org/codehaus/gmavenplus/mojo/AbstractGroovyMojoTest.java

sed -i.orig 's|\r||g' README.markdown
touch -r README.markdown.orig README.markdown

%mvn_file : gmavenplus-plugin

%build

%mvn_build -f -- -Pnonindy

%install
%mvn_install

%files -f .mfiles
%doc README.markdown LICENSE.txt

%files help -f .mfiles-javadoc

%changelog
* Tue Dec 03 2024 Ge Wang <wang__ge@126.com> - 3.0.2-1
- Update to version 3.0.2

* Wed Jun 19 2024 Ge Wang <wang__ge@126.com> - 2.1.0-1
- Update to version 2.1.0

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.5-9
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jun 3 2021 baizhonggui <baizhonggui@huawei.com> - 1.5-8
- Fix building error: /usr/bin/git: No such file or directory
- Add git in BuildRequires

* Fri Sep 11 2020 zhanghua <zhanghua40@huawei.com> - 1.5-7
- Disable tests due to incompatibility with new mockito

* Thu Apr 2 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.5-6
- Package init.

